import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constant } from 'src/app/common/constant.enum';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AllProductsService {
 


  constructor(private http: HttpClient) { }
 
  getAllProducts(): Observable<any> {
    return this.http.get(Constant.API_URL + 'allproducts/', {})
      .pipe(map((res: any) => res));
  }

  
  AddParticular(obj: object): Observable<any>  {
    let headers = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem("token") });
    console.log(headers)
    return this.http.post(Constant.API_URL +'wishlist/',{'product_id':obj},{ headers: headers})
      .pipe(map((res: any) => res));
  }
  getAllCartProducts(): Observable<any> {
    let headers = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem("token") });
    return this.http.get(Constant.API_URL + 'allcart/',{ headers: headers})
      .pipe(map((res: any) => res));
  }


}

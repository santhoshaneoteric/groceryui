import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor() { }
  getNavItems() {
    return [
      {
        navIcon: 'home',
        navLabel: 'Home'
      },
      {
        navIcon: 'flight',
        navLabel: 'Flights'
      },
      {
        navIcon: 'hotel',
        navLabel: 'Hotels'
      },
      {
        navIcon: 'local_mall',
        navLabel: 'Holidays'
      },
      {
        navIcon: 'local_offer',
        navLabel: 'Offers'
      },
      {
        navIcon: 'contacts',
        navLabel: 'Contact Us'
      }
    ];
  }


  
  getImagesObject() {
    return [{
      image: '../../assets/images/beach.png',
      thumbImage: '../../assets/images/beach.png',
      alt: 'alt of image',
      title: 'Up to INR 10,000 Instant Discount* on GoAir Flight...'
    }, {
        image: '../../assets/images/1_holiday.jpg',
        thumbImage: '../../assets/images/1_holiday.jpg',
        title: 'Up to INR 1,800 instant discount on Domestic Hotel...',
        alt: 'Image alt'
    }, {
      image: '../../assets/images/girl.png',
      thumbImage: '../../assets/images/girl.png',
      alt: 'alt of image',
      title: 'title of image'
    }, {
      image: '../../assets/images/beach.png',
      thumbImage: '../../assets/images/beach.png',
      title: 'Image title',
      alt: 'Image alt'
    }];
  } 

  
  getInnoDemos() {
    return [{
      image: '../../assets/images/1.jpg',
      title: 'Colombo',
      description: 'Explore Colombo in Under Rs. 25,000'
    }, {
      image: '../../assets/images/2.jpg',
      title: 'Holidays',
      description: 'Holidays Under 10K This February'
    }, {
      image: '../../assets/images/3.jpg',
      title: 'Visa',
      description: 'List of Countries Offering Visa on Arrival'
    },
     {
      image: '../../assets/images/4.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/5.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/6.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/7.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/8.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/9.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    }
    ,
    {
      image: '../../assets/images/10.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    },
    {
      image: '../../assets/images/11.jpg',
      title: 'Destinations',
      description: 'Top 6 Most Romantic Honeymoon Destinations'
    }
  ];
  }
  getFooterObj() {
    return [{
      title: 'Tripee',
      content: ['Careers', 'News', 'Policies', 'Diversity & Belonging', 'Accessibility']
    }, {
      title: 'Discover',
      content: ['Trust & Safety', 'Travel Credit', 'Tripee Citizen', 'Business Travel', 'Things To Do']
    }, {
      title: 'Hosting',
      content: ['Why Host', 'Hospitality', 'Responsible Hosting', 'Community Centre', 'Host an Experience', 'Open Homes']
    }, {
      title: 'Support',
      content: ['Help']
    }];
  }
}

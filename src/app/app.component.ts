import { Component, Output, Input, EventEmitter } from '@angular/core';
import { HelperService } from 'src/app/common/helper.service';
import {AllProductsService} from 'src/app/productlist/all-products.service'
import { Router, NavigationEnd } from '@angular/router';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { HttpHeaders } from '@angular/common/http';
import {SignupComponent} from 'src/app/signup/signup.component'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'List';
  innoDemos = [];
  ProdcutListArr:any=[];
  headers: HttpHeaders;
  constructor(private dialog: MatDialog,private router: Router,private productService: AllProductsService) { }
  
 
  ngOnInit() {
    console.log("came to init")
    this.productService.getAllProducts().subscribe(data => {
      console.log(data)
     
      if (data) {
      //  this.spinner.hide();
        sessionStorage.setItem('resultData', JSON.stringify(data));
        console.log(sessionStorage.getItem('resultData'))
        
        //this.router.navigateByUrl('productlist');
      } else {
        console.log("There Is no Data")
       // this.spinner.hide();
      }
    });
    this.ProdcutListArr=JSON.parse(sessionStorage.getItem('resultData'));
    console.log(this.ProdcutListArr)
    // this.ProdcutListArr.forEach(element => {
      
    // });

  }
  
  addProductToCart(product){
    console.log(product)
    // this.headers = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem("token") });
    // console.log(this.headers)
    this.productService.AddParticular(product.id).subscribe(data => {
      console.log(data)
     
      if (data) {
      //  this.spinner.hide();
        sessionStorage.setItem('SingleData', JSON.stringify(data));
        console.log(sessionStorage.getItem('SingleData'))
        
        //this.router.navigateByUrl('productlist');
      } else {
        console.log("There Is no Data")
       // this.spinner.hide();
      }
    });
  }


  openLoginDialog() {
    const dialogRef = this.dialog.open(LoginDialogComponent ,{
      // disableClose: true,
      panelClass: 'login-container'
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      if (data) {
        // this.userLoggedIn = true;
        // this.userDetails = data;
      localStorage.setItem('userExists', JSON.stringify(data));
      }
    });
  }
  openSignupDialog() {
    const dialogRef = this.dialog.open(SignupComponent ,{
     // disableClose: true,
      panelClass: 'login-container'
    });
    dialogRef.afterClosed().subscribe(data => {
      console.log(data);
      if (data) {
        // this.userLoggedIn = true;
        // this.userDetails = data;
        // localStorage.setItem('userExists', JSON.stringify(data));
      }
    });
  }
  OnCartProducts(){
    this.productService.getAllCartProducts().subscribe(data => {
      console.log(data)
     
      if (data) {
     
        sessionStorage.setItem('CartData', JSON.stringify(data));
        console.log(sessionStorage.getItem('CartData'))
        
        this.router.navigateByUrl('productlist');
      } else {
        console.log("There Is no Data")
       
      }
    });
    this.ProdcutListArr=JSON.parse(sessionStorage.getItem('resultData'));
    console.log(this.ProdcutListArr)
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductlistComponent } from './productlist/productlist.component';

const routes: Routes = [
  { path: '',
redirectTo: '',
pathMatch: 'full'
}, {
path: 'products',
component: ProductlistComponent
},

  // ,{
  //   path:'productlist',
  //   component:ProductlistComponent
  // },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 

}

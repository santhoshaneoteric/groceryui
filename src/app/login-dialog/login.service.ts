import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Constant } from 'src/app/common/constant.enum';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http :HttpClient) { }
  login(obj: object): Observable<any>  {
    return this.http.post(Constant.API_URL + 'login/', obj, {})
      .pipe(map((res: any) => res));
  }
  create(obj: object): Observable<any>  {
    return this.http.post(Constant.API_URL + 'register/', obj, {})
      .pipe(map((res: any) => res));
  }
}

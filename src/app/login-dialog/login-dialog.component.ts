import { Component, OnInit } from '@angular/core';

import { MatDialog, MatDialogRef } from '@angular/material/dialog';


import { LoginService } from './login.service';

import {SignupComponent} from 'src/app/signup/signup.component'

import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Constant } from 'src/app/common/constant.enum';
import { CommonService } from 'src/app/common/common.service';
//import { JwtHelperService } from '@auth0/angular-jwt';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  showFormFields = false;
  loginForm: FormGroup;
  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<LoginDialogComponent>,
              private loginService: LoginService,  private fb: FormBuilder,
              private router: Router,private commonService:CommonService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({

      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
     
    });
  }

  openSignupDialog() {
    this.dialog.open(SignupComponent, {
      disableClose: true,
      panelClass: 'login-container'
    });
  }

  loginDialogClose() {
    this.dialogRef.close();
  }

  onTripeeAccount() {
   // this.showFormFields = true;
   
  
}

  onPersonalAccount() {
    this.showFormFields = false;
  }

  onlogin() {
    // tslint:disable-next-line: no-string-literal
   // const encryptedPswd = this.encrypt(this.loginForm.value['password']);
    this.loginService.login({email: this.loginForm.value.email, password: this.loginForm.value.password}).subscribe((data) => {
      if (data) {
        console.log(data.token);
        console.log(data.name);
         localStorage.setItem("token", data.token);
         localStorage.setItem("username", data.name);
        console.log(localStorage.getItem('token'))
      
        
        this.dialogRef.close();
        //this.router.navigateByUrl('home');

      }
    });
  }
 
}

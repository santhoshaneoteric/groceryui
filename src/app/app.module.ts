import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { HttpClientModule } from '@angular/common/http';
import { LoginDialogComponent } from './login-dialog/login-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {SharedModule} from 'src/app/shared/shared.module';
import { SignupComponent } from './signup/signup.component'
@NgModule({
  declarations: [
    AppComponent,
    ProductlistComponent,
    LoginDialogComponent,
    SignupComponent,
   
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    SharedModule,
    ReactiveFormsModule,
    MatInputModule

  ],
  providers: [],
  entryComponents: [LoginDialogComponent,SignupComponent],

  bootstrap: [AppComponent]
})
export class AppModule { }

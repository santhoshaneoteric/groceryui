import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginDialogComponent } from 'src/app/login-dialog/login-dialog.component';
import { LoginService } from 'src/app/login-dialog/login.service';

import { Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signupForm: FormGroup;
  constructor(private dialog: MatDialog, private dialogRef: MatDialogRef<SignupComponent>,
              private fb: FormBuilder, private signupService: LoginService,private router:Router) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      username: ['', Validators.required],
      
      email: ['', [
        Validators.required,
        Validators.pattern('^[A-Za-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
      ]],
      password:['', [Validators.required, Validators.minLength(6)]],
      
    });
  }

  // openLoginDialog() {
  //   this.dialog.open(LoginDialogComponent, {
  //       disableClose: true,
  //       panelClass: 'login-container'
  //   });
  // }

  signupDialogClose() {
    this.dialog.closeAll();
  }

  onCreateAccount() {
    console.log(this.signupForm.value);
    this.signupService.create(this.signupForm.value).subscribe(data => {
      this.dialog.closeAll();
      if (data) {
        console.log(data)

        //console.log(data.email)
        localStorage.setItem('email', data.email);
       // this.commonService.saveUserInfo(data.data);

        this.dialogRef.close(data);
        this.dialog.open(LoginDialogComponent, {
          disableClose: true,
          panelClass: 'login-container'
      });
    



      }
      else{
       localStorage.setItem('error',data.email)
        

      }
    });
  }
}
